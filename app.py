from flask import Flask
from gradio import Interface
from chatbot import Chatbot

def init_app():
    """Initialize the Flask application."""
    app = Flask(__name__)
    return app

def run():
    """Start the Flask server and serve the Gradio interface."""
    app = init_app()
    chatbot = Chatbot()
    iface = Interface(chatbot.converse, "textbox", "textbox")
    iface.launch()

if __name__ == "__main__":
    run()
