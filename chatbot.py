from utils import get_response, speak, listen

class Chatbot:
    def __init__(self):
        """Initialize the chatbot, setting up the necessary services."""
        pass

    def converse(self, input):
        """Handle a single round of conversation, taking in user input, retrieving the AI's response, and returning the response."""
        if input.startswith("speak:"):
            input = listen()
        response = get_response(input)
        speak(response)
        return response
