Flask==2.0.2
openai==0.27.0
gradio==2.3.9
langchain==0.1.2
pyttsx3==2.90
SpeechRecognition==3.8.1
pymemgpt==0.1.1
