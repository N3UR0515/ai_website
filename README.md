Based on the provided specifications, we will create the following files:

1. `requirements.txt`: This file will contain all the Python library dependencies that need to be installed.
2. `run.sh`: This is the entry point of the program. It will start the Flask server and serve the Gradio interface.
3. `app.py`: This is the main Flask application file. It will contain the `init_app()` and `run()` functions.
4. `chatbot.py`: This file will contain the `Chatbot` class, including the `__init__()` and `converse()` methods.
5. `utils.py`: This file will contain the `get_response()`, `speak()`, and `listen()` functions.
6. `README.md`: This file will be updated with details about the project, including its purpose, functionality, and instructions for setup and use.

Now, let's write the content for each file.

**requirements.txt**
